## Description
 This is a  Secret note Backend API Service

## Features

  - **Integrated ESLint, Prettier and Husky**
  - **Validation: request data validation using Nest JS Pipe**
  - **Logging: using winston**
  - **Testing: unit and integration tests using Jest**
  - **API documentation: with swagger**
  - **Dependency management: with yarn**
  - **Environment variables: using dotenv**
  - **Security: set security HTTP headers using helmet**
  - **CORS: Cross-Origin Resource-Sharing enabled using cors**
  - **CI: Continuous integration with Gitlab CI**
  - **Docker support**
  - **Git hooks: with husky and lint-staged**
  - **Linting: with ESLint and Prettier**
  - **Prisma: For database connection,schema**

## Installation

```bash
$ yarn install
```

# Create The Environment Variables && Private Key


Create .env file

```env
DATABASE_URL=postgresql://postgres:password@localhost:5432/database_name?connect_timeout=300
```

To generate private key
```bash
$ make generate-key
```
### Running Application

To start the docker container, postgresql container and migrate the Existing db schema.

```bash
$ make start:dev

# running at http://localhost:3000/api/v1/note     🚀🚀🚀 

```

# API Documentation
The web api documentation was developed with swagger. To access the documentation visit
<a href="http://localhost:3000/api/docs">Swagger API DOC</a>



## Migration
You need to migrate the schema to database
```bash
$ make migration
```
- To create migration for a new schema.
```bash
 yarn migration:create
```
the application should be running at localhost:3000


## Test

The test suite runs a seperate  db docker instance. It can be run with the script below.
```bash
$ yarn test:int or make test-app
```



## Gitlab CI/CD
 
  -  gitlab-ci-cd.yml is  ci/cd file

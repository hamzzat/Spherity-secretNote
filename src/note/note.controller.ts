import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  Query,
} from '@nestjs/common';
import { NoteService } from './note.service';
import { CreateNoteDto } from './dto/create-note.dto';
import { UpdateNoteDto } from './dto/update-note.dto';
import { ApiBody, ApiTags } from '@nestjs/swagger';
import { secretNote as secretNoteModel } from '@prisma/client';
import { PinoLogger } from 'nestjs-pino';

@Controller('/api/v1/note')
@ApiTags('notes')
export class NoteController {
  constructor(private readonly noteService: NoteService,private readonly logger: PinoLogger) {}

  @Post()
  @ApiBody({ type: CreateNoteDto })
  async create(@Body() createNoteDto: CreateNoteDto): Promise<secretNoteModel> {
        try {
         return await this.noteService.create(createNoteDto);
        }catch(e){
      this.logger.error(e);
      throw e;
    }
  }

  @Get()
  async findAll(
    @Query('decrypted') decrypted?: boolean,
  ): Promise<secretNoteModel[]> {
    try{
    return await this.noteService.findAll(decrypted);
    }catch(e){
      this.logger.error(e);
      throw e;
    }
  }

  @Get(':id')
  findOne(
    @Param('id') id: number,
    @Query('decrypted') decrypted?: boolean,
  ): Promise<secretNoteModel> {
    decrypted = decrypted || false;
    try{
    
    return this.noteService.findOne(+id, decrypted);
    }catch(e){
      this.logger.error(e);
      throw e;
    }
  }

  @Patch(':id')
  @ApiBody({type: UpdateNoteDto})
  update(@Param('id') id: number, @Body() updateNoteDto: UpdateNoteDto) {
    return this.noteService.update(+id, updateNoteDto);
  }

  @Delete(':id')
  remove(@Param('id') id: number): Promise<secretNoteModel> {
    return this.noteService.remove(+id);
  }
}

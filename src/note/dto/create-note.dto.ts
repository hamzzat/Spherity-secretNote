import { ApiProperty } from '@nestjs/swagger';
import {IsString, Validate} from 'class-validator';
import { NotEmptyIfEmptyString } from 'src/core/utils';
export class CreateNoteDto {
  
  @IsString()
  @Validate(NotEmptyIfEmptyString)
  @ApiProperty()
  note: string;
}

import { secretNote } from '@prisma/client';
import { ApiProperty } from '@nestjs/swagger';
export class NoteEntity implements secretNote {
 
  @ApiProperty()
  id: number;

  @ApiProperty({ required: true })
  note: string;
  
  @ApiProperty()
  createdAt: Date;

  @ApiProperty()
  updatedAt: Date;
}

import {
  createCipheriv,
  createDecipheriv,
  createHash,
  randomBytes,
} from 'crypto';
import {
  ValidatorConstraint,
  ValidatorConstraintInterface,
} from 'class-validator';
import {readFileSync} from "fs";

const path = process.cwd();
const buffer: string = readFileSync(path + "/key/private.key", "utf8") || process.env.SECRET_KEY;
const ALGORITHM  = 'aes-256-gcm';
const key: string = createHash('sha256')
  .update(String(buffer.toString()))
  .digest('base64')
  .substring(0, 32);

  // encrypt data
export function encrypt(str: string): string {
  try{
  const initVector : Buffer = randomBytes(16);
  const initVectorHex: string = initVector.toString('hex');
  const cipher = createCipheriv(ALGORITHM, key, initVector);
  const encoded : string = cipher.update(str, 'utf-8', 'hex') + cipher.final('hex');
  const authTag: string  = cipher.getAuthTag().toString('hex');
  const metaAndEncoded: string = [authTag, initVectorHex, encoded].join('|');
  return metaAndEncoded;
} catch (exception) {
  throw new Error(exception);
}
}
  // decrypt data
export function decrypt(str: string): string {
  try{
  const [authTag, initVectorHex, encrypted] = str.split('|');
  const initVector : Buffer = Buffer.from(initVectorHex, 'hex');
  const decipher = createDecipheriv(ALGORITHM, key, initVector);
  decipher.setAuthTag(Buffer.from(authTag, 'hex'));
  const decrypted : string =
    decipher.update(encrypted, 'hex', 'utf-8') + decipher.final('utf-8');
  return decrypted;
  } catch (exception) {
  throw new Error(exception);
}
}



// Validator functions to validate Empty string of "" and " "
@ValidatorConstraint({ name: 'notEmptyIfEmptyString', async: false })
export class NotEmptyIfEmptyString implements ValidatorConstraintInterface {
  validate(value: any) {
    const isEmptyString = value === '' || value === " " || value === null || value === undefined || value.length === 0;

    return !isEmptyString;
  }

  defaultMessage() {
    return 'Value must not be empty';
  }
}

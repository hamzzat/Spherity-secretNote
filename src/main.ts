import { HttpAdapterHost, NestFactory } from '@nestjs/core';
import { ValidationPipe } from '@nestjs/common';
import { AppModule } from './app.module';
import { SwaggerModule, DocumentBuilder } from '@nestjs/swagger';
import { PrismaService } from './core/prisma/prisma.service';
import { PrismaClientExceptionFilter } from '../src/core/prisma-client-exception/prisma-client-exception.filter';
import { useContainer } from 'class-validator';
import helmet from 'helmet';
import { LoggerErrorInterceptor, Logger } from 'nestjs-pino';

async function bootstrap() {
  const app = await NestFactory.create(AppModule, { bufferLogs: true })

  const config = new DocumentBuilder()
    .setTitle('Secrete Notes API')
    .setDescription('API Documentation for secret note applications')
    .setVersion('1.0')
    .addTag('Notes')
    .build();
  const document = SwaggerModule.createDocument(app, config);
  SwaggerModule.setup('api/docs', app, document);
  app.useGlobalPipes(new ValidationPipe({    transform: true,}));
  app.useLogger(app.get(Logger));
  app.useGlobalInterceptors(new LoggerErrorInterceptor());
  app.enableCors();
  app.use(helmet());
  const prismaService = app.get(PrismaService);
  await prismaService.enableShutdownHooks(app);
  const { httpAdapter } = app.get(HttpAdapterHost);
  useContainer(app.select(AppModule), { fallbackOnErrors: true });
  app.useGlobalFilters(new PrismaClientExceptionFilter(httpAdapter));

  await app.listen(3000);
}
bootstrap();



